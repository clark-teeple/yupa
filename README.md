This app is a tool that monitors municipal zoning change notices for natural land, detects size and natural features in the land, and notifies a set of contacts so they can determine if the land can be protected by slowing or even stopping development.

Users can input addresses to monitor on regions we've already implemented, or submit a request to cover a region. They can also sign up for alerts for any given address or region.

This is named after Lord Yupa from Nausicaa of the Valley of the Wind

Features:
  - Map of monitored addresses for a given region
  - Map of change proposals for a given region
  - Region request, address, alert submissions
  - List of implemented regions, requested regions
  - Land action contact list - only accessible to registered users

To start, run `docker-compose up`.

TODOs:

Add unique constraint to subscriptions
test and respond no matches
test and respond no contacts
test and respond no page response
test and respond no addresses for region/page
test and handle multiple matches
Test actual scraping
Error handling
Set up scheduling
Set up sendgrid or other service
Deploy
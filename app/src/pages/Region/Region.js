import React from 'react';
import { makeStyles, Grid } from "@material-ui/core"
import './Region.css';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

function Region() {
  const classes = useStyles();

  return ( 
    <Grid container spacing={3}>
      <Grid item xs={12}>
        Region
      </Grid>
    </Grid>
  );
}

export default Region;
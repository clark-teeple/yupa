import React from 'react';
import { makeStyles, Grid } from "@material-ui/core"
import './Home.css';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

function Home() {
  const classes = useStyles();

  return ( 
    <Grid container spacing={3}>
      <Grid item xs={12}>
        Home
      </Grid>
    </Grid>
  );
}

export default Home;
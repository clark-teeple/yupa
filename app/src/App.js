import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import { makeStyles, Grid } from "@material-ui/core";
import './App.css';

import Nav from "./components/Nav/Nav.js";
import Region from "./pages/Region/Region.js";
import Home from "./pages/Home/Home.js";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}))

function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Nav />
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/region">
            <Region />
          </Route>
          <Route exact path="/region/:region">
            <div> Region ID </div>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

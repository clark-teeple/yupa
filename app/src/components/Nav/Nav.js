import React from 'react';
import { makeStyles, Grid } from "@material-ui/core"
import logo from "../../logo.jpg";
import colours from "../../constants/colours.js";

import './Nav.css';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  nav: {
    borderBottom: `${colours.green} solid 3px`
  },
  logo: {
    height: 128,
    width: 256
  }
}));

function Nav() {
  const classes = useStyles();

  return ( 
    <Grid container spacing={3}>
      <Grid container item xs={12} className={classes.nav}>
        <Grid item xs={3}>
          <img src={logo} className={classes.logo} />
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Nav;
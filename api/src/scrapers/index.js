// this module will house all the custom scrapers for municipal websites

// regions will contain a list of pages to scrape
// basically all it has to do is get the page and
// run a search for each address associated with the region
// so every Friday or something, the scheduler will:
// select region
// iterate through the regions
// for each region, select all addresses
// iterate through region pages
// iterate through addresses and search for each on page
// return any matches
// email contacts list for each match

// users can sign up to get notifications for entire regions

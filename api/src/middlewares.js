const config = require("./config");
const jwt = require("express-jwt");

// When X-Forwarded-Proto is set to http, redirect to https
const redirectHTTPS = (req, res, next) => {
  if (
    config.api.PROTOCOL === "https"
    && req.header("X-Forwarded-Proto") === "http"
  ) {
    res.redirect(
      `${config.api.PROTOCOL}://${req.header("host")}${req.originalUrl}`
    );
    return;
  }
  next();
};

const corsConfig = {
  origin: (origin, callback) => {
    if (config.env === "production") {
      // Not enabled in dev
      if (
        !origin
        || config.api.ALLOWED_ORIGINS.indexOf(origin) !== -1
        || config.api.ALLOWED_ORIGINS.includes("*")
        || origin.match(
          `^${config.api.PROTOCOL}://.*[.]${config.api.ALLOWED_ORIGIN_APEX}$`
        )
      ) {
        callback(null, true);
      } else {
        callback(new Error(`CORS error: ${origin}`));
      }
    } else {
      callback(null, true);
    }
  },
  credentials: true
};

const excludedPaths = [
  `${config.api.PATH_PREFIX}/account`,
  `${config.api.PATH_PREFIX}/account/login`
];

const isAuthed = jwt({
  secret: config.auth.TOKEN_KEY,
  algorithms: ["HS256"],
  credentialsRequired: false,
  getToken: (req) => {
    if (req.headers.authorization) {
      const parsed = req.headers.authorization.match(/Bearer (.*)/);
      if (parsed && parsed[1]) {
        return parsed[1];
      }
    }
    return null;
  }
}).unless({
  path: excludedPaths
});

const authErrorHandler = (err, req, res, next) => { // eslint-disable-line consistent-return
  if (err.name === "UnauthorizedError") {
    return res.status(401).end();
  }
  next(err);
};

module.exports = {
  redirectHTTPS,
  corsConfig,
  isAuthed,
  authErrorHandler
};

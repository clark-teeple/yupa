module.exports = {
  api: {
    PORT: process.env.PORT || 3001,
    PROTOCOL: process.env.PROTOCOL || "http://",
    PATH_PREFIX: process.env.PATH_PREFIX || "/api",
    ALLOWED_ORIGINS: process.env.ALLOWED_ORIGINS
      ? process.env.ALLOWED_ORIGINS.split(",")
      : ["http://localhost:3000"]
  },
  db: {
    PG_HOST: process.env.PG_HOST || "localhost",
    PG_PORT: process.env.PG_PORT || 5434,
    PG_USERNAME: process.env.PG_USERNAME || "yupa",
    PG_PASSWORD: process.env.PG_PASSWORD || "yupa",
    PG_DATABASE: process.env.PG_DATABASE || "yupa",
    get pgConnectionString() {
      return `postgres://${this.PG_USERNAME}:${this.PG_PASSWORD}@${this.PG_HOST}:${this.PG_PORT}/${this.PG_DATABASE}`;
    }
  },
  auth: {
    TOKEN_KEY: process.env.TOKEN_KEY || "default"
  },
  env: process.env.NODE_ENV || "development"
};

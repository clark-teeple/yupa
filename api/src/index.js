const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const cors = require("cors");

const {
  corsConfig,
  redirectHTTPS,
  isAuthed,
  authErrorHandler
} = require("./middlewares");

const config = require("./config");
const routes = require("./routes");

const app = express();

// Use helmet and body-parser to parse json bodies
app.use(bodyParser.urlencoded({ limit: "50MB", extended: true }));
app.use(bodyParser({ limit: "50MB" }));
app.use(helmet());

app.use(cors(corsConfig));
app.use(redirectHTTPS);

app.use(isAuthed);
app.use(authErrorHandler);
app.use(config.api.PATH_PREFIX, routes.router);

const server = app.listen(config.api.PORT, (err) => {
  if (err) {
    console.error(`Could not listen on port ${config.api.PORT}`);
    process.exit(1);
  } else {
    console.info(
      `${Date()}: Running API in ${config.env} mode on ${
        config.api.PORT
      } accepting traffic from origins ${config.api.ALLOWED_ORIGINS.toString()}`
    );
  }
});

// Gracefully exit api
function apiClose() {
  server.close();
  process.exit(0);
}

// Handle signals
process.on("SIGTERM", apiClose);
process.on("SIGINT", apiClose);

module.exports = app;

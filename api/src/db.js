const config = require("./config");

// Create one connection pool to use throughout
const knex = require("knex")({
  client: "postgres",
  connection: config.db.pgConnectionString,
  searchPath: ["knex", "public"],
  debug: !(process.env.NODE_ENV === "production"),
  pool: { min: 0, max: 30 }
});

const db = knex;

module.exports = {
  db
};

const { db } = require("../db");

const axios = require("axios");

const getRegionPages = async (regionId) => db("region")
  .select({
    id: "region.id",
    name: "region.name",
    url: "page.url"
  })
  .innerJoin(
    "page",
    "page.region",
    "region.id"
  )
  .where({
    "region.id": regionId
  });

const getAddresses = async (regionId) => db("address")
  .select({
    address: "address.address",
    id: "address.id"
  })
  .where({
    "address.region": regionId
  });

const getRegionContacts = async (regionId) => db("region_account")
  .select({
    email: "account.email",
    firstName: "account.first_name",
    lastName: "account.last_name"
  })
  .innerJoin(
    "account",
    "account.id",
    "region_account.account"
  )
  .where({
    "region_account.region": regionId
  });

const getAddressContacts = async (addressId) => db("address_account")
  .select({
    email: "account.email",
    firstName: "account.first_name",
    lastName: "account.last_name"
  })
  .innerJoin(
    "account",
    "account.id",
    "address_account.account"
  )
  .where({
    "address_account.address": addressId
  });

const contactContact = (contact, address, region) => {
  const message = `an email was sent to ${contact} regarding ${address} in ${region}`;
  // will use async when actually emailing
  return new Promise((resolve) => {
    console.info(message);
    resolve({ contact, address, region });
  });
};

const runRegion = async (regionId) => {
  const pages = await getRegionPages(regionId);
  if (!pages.length > 0) {
    return `No region or pages found for Region ID ${regionId}`;
  }
  const addresses = await getAddresses(regionId);

  const pagePromises = pages.map(async (regionPage) => {
    const markup = axios.get(regionPage.url);
    return markup;
  });
  const pageResponses = await Promise.all(pagePromises);

  const matches = addresses.filter((address) => {
    const isAddressMatched = pageResponses.filter((page) => {
      const isPageMatched = page.data.split(address.address).length - 1 > 0;
      return isPageMatched;
    }).length > 0;
    return isAddressMatched;
  });

  const regionContacts = await getRegionContacts(regionId);
  const addressContactResults = await Promise.all(
    matches.map(async (match) => {
      const addressContacts = await getAddressContacts(match.id);
      return addressContacts;
    })
  );

  const addressContacts = addressContactResults.length ? (
    addressContactResults[0].filter((contact) => {
      const hasContacts = regionContacts.findIndex((comp) => comp.email === contact.email) < 0;
      return hasContacts;
    })
  ) : (
    []
  );

  const allContacts = [
    ...regionContacts,
    ...addressContacts
  ];

  const matchPromises = matches.map(async (match) => {
    await Promise.all(allContacts.map((contact) => {
      const contactResult = contactContact(contact.email, match.address, pages[0].name);
      return contactResult;
    }));

    return match;
  });

  return Promise.all(matchPromises);
};

module.exports = {
  runRegion
};

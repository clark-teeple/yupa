const express = require("express");

const { db } = require("../db");

const router = express.Router();

router.get(
  "/",
  async (req, res) => {
    await db("address")
      .select("*")
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.get(
  "/:id",
  async (req, res) => {
    await db("address")
      .select("*")
      .where(
        "id",
        req.params.id
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.post(
  "/",
  async (req, res) => {
    const transaction = await db.transaction();
    await db("address")
      .transacting(transaction)
      .returning("*")
      .insert({
        address: req.body.address,
        created_by: req.user.id,
        region: req.body.region
      })
      .then((data) => {
        transaction.commit();
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.post(
  "/subscribe",
  async (req, res) => {
    const transaction = await db.transaction();
    await db("address_account")
      .transacting(transaction)
      .returning("*")
      .insert({
        address: req.body.address,
        account: req.user.id
      })
      .then((data) => {
        transaction.commit();
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

module.exports = {
  router
};

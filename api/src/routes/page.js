const express = require("express");

const { db } = require("../db");

const router = express.Router();

router.get(
  "/",
  async (req, res) => {
    await db("page")
      .select("*")
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.get(
  "/:id",
  async (req, res) => {
    await db("page")
      .select("*")
      .where(
        "id",
        req.params.id
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.post(
  "/",
  async (req, res) => {
    const transaction = await db.transaction();
    await db("page")
      .transacting(transaction)
      .withSchema("public")
      .returning("*")
      .insert({
        region: req.body.region,
        url: req.body.url
      })
      .then((data) => {
        transaction.commit();
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

module.exports = {
  router
};

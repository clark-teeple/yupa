const express = require("express");

const { db } = require("../db");

const router = express.Router();

router.get(
  "/",
  async (req, res) => {
    await db("region")
      .select("*")
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.get(
  "/:id",
  async (req, res) => {
    await db("region")
      .select("*")
      .where(
        "id",
        req.params.id
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.post(
  "/",
  async (req, res) => {
    const transaction = await db.transaction();
    await db("region")
      .transacting(transaction)
      .withSchema("public")
      .returning("*")
      .insert({
        name: req.body.name
      })
      .then((data) => {
        transaction.commit();
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);

router.post(
  "/subscribe",
  async (req, res) => {
    const transaction = await db.transaction();
    await db("region_account")
      .transacting(transaction)
      .returning("*")
      .insert({
        region: req.body.region,
        account: req.user.id
      })
      .then((data) => {
        transaction.commit();
        res.status(200).send(data);
      })
      .catch((e) => {
        res.status(500).send(e);
      });
  }
);
module.exports = {
  router
};

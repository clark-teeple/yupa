const express = require("express");
const jwt = require("jsonwebtoken");
const { db } = require("../db");
const config = require("../config");

const router = express.Router();

router.post(
  "/login",
  async (req, res) => {
    try {
      const account = await db.raw(
        "SELECT id, email, first_name as firstName, last_name as lastName FROM account WHERE email = ? AND password = crypt(?, password);", [req.body.email, req.body.password]
      );
      if (!account.rows[0]) res.status(200).send("Incorrect email or password");
      jwt.sign(account.rows[0], config.auth.TOKEN_KEY, { expiresIn: "1h" }, (err, token) => {
        if (err) res.status(500).send("Token Error");
        res.status(200).send(token);
      });
    } catch (err) {
      res.status(500).send(err);
    }
  }
);

router.post(
  "/",
  async (req, res) => {
    try {
      const newAccount = await db.raw(
        "INSERT INTO account (email, password, first_name, last_name) VALUES (?, crypt(?, gen_salt('bf')), ?, ?) RETURNING email, id;",
        [req.body.email, req.body.password, req.body.firstName, req.body.lastName]
      );
      res.status(200).send(newAccount.rows[0]);
    } catch (err) {
      let message = "Something went wrong";
      if (err.code === "23505") message = "A user with this email already exists";
      res.status(500).send(message);
    }
  }
);

module.exports = {
  router
};

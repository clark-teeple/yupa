const express = require("express");

const { runRegion } = require("../scheduling");

const router = express.Router();

router.get(
  "/test",
  (req, res) => {
    res
      .status(200)
      .send("received");
  }
);

router.post(
  "/test",
  (req, res) => {
    res
      .status(200)
      .send(req.body);
  }
);

router.get(
  "/runRegion",
  async (req, res) => {
    await runRegion(1).then((data) => {
      res
        .status(200)
        .send(data);
    }).catch((e) => {
      res
        .status(500)
        .send(e);
    });
  }
);

module.exports = {
  router
};

const express = require("express");

const router = express.Router();

const healthchecks = require("./healthcheck");
const address = require("./address");
const region = require("./region");
const admin = require("./admin");
const account = require("./account");
const page = require("./page");

router.use("/healthcheck", healthchecks.router);
router.use("/address", address.router);
router.use("/region", region.router);
router.use("/admin", admin.router);
router.use("/account", account.router);
router.use("/page", page.router);

module.exports = {
  router
};

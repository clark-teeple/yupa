const express = require("express");

const router = express.Router();

const { runRegion } = require("../scheduling");

router.get(
  "/runRegion/:id",
  async (req, res) => {
    await runRegion(req.params.id).then((result) => {
      res.status(200).send(result);
    }).catch((e) => {
      res.status(500).send(e);
    });
  }
);

module.exports = {
  router
};

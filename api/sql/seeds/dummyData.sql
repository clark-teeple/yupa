INSERT INTO region (
  id,
  name
) VALUES (
  1,
  'test region'
);

INSERT INTO page (
  url,
  region
) VALUES (
  'http://www.google.ca',
  1
), (
  'http://news.ycombinator.com',
  1
);

INSERT INTO account (
  id,
  first_name,
  last_name,
  email,
  password
) VALUES (
  1,
  'test',
  'test',
  'test@test.com',
  crypt('Test12345%', gen_salt('bf'))
);

INSERT INTO address (
  address,
  created_by,
  region
) VALUES (
  'test address',
  1,
  1
);

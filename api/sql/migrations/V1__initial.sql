CREATE EXTENSION IF NOT EXISTS citext;
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS account (
  id uuid DEFAULT uuid_generate_v4() primary key,
  first_name text not null,
  last_name text not null,
  email citext not null UNIQUE,
  password text not null
);

CREATE TABLE IF NOT EXISTS region (
  id uuid DEFAULT uuid_generate_v4() primary key,
  name text not null UNIQUE,
  municipal_website text not null
);

CREATE TABLE IF NOT EXISTS address (
  id uuid DEFAULT uuid_generate_v4() primary key,
  address text not null UNIQUE,
  created_by uuid not null,
  region uuid not null,
  FOREIGN KEY (created_by) REFERENCES account (id),
  FOREIGN KEY (region) REFERENCES region (id)
);

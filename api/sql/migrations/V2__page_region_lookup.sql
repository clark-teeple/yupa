CREATE TABLE IF NOT EXISTS page (
  id uuid DEFAULT uuid_generate_v4() primary key,
  url text not null UNIQUE,
  region uuid not null,
  FOREIGN KEY (region) REFERENCES region (id)
);

ALTER TABLE region DROP COLUMN municipal_website;
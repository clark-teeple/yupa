CREATE TABLE IF NOT EXISTS address_account (
  id uuid DEFAULT uuid_generate_v4() primary key,
  account uuid not null,
  address uuid not null,
  FOREIGN KEY (account) REFERENCES account (id),
  FOREIGN KEY (address) REFERENCES address (id)
);

CREATE TABLE IF NOT EXISTS region_account (
  id uuid DEFAULT uuid_generate_v4() primary key,
  account uuid not null,
  region uuid not null,
  FOREIGN KEY (account) REFERENCES account (id),
  FOREIGN KEY (region) REFERENCES region (id)
);